package main

import (
	"fmt"
	"goalfeed/utils"

	// iihfClients "goalfeed/clients/leagues/iihf"
	mlbClients "goalfeed/clients/leagues/mlb"
	nhlClients "goalfeed/clients/leagues/nhl"
	"goalfeed/models"
	"goalfeed/services/leagues"
	// "goalfeed/services/leagues/iihf"
	"goalfeed/services/leagues/mlb"
	"goalfeed/services/leagues/nhl"
	"goalfeed/targets/database"
	"goalfeed/targets/pusher"
	"goalfeed/targets/redis"
	"os"
	"sync"
	"time"

	"github.com/bugsnag/bugsnag-go/v2"
	"github.com/joho/godotenv"
)

var leagueServices = map[int]leagues.ILeagueService{}
var needRefresh = false
var logger = utils.GetLogger()

func init() {
	_ = godotenv.Load()

	stage := os.Getenv("RELEASE_STAGE")
	if len(stage) > 0 {
		stage = "development"
	}
	apiKey := os.Getenv("BUGSNAG_API_KEY")
	if len(apiKey) > 0 {
		bugsnag.Configure(bugsnag.Configuration{
			APIKey:       apiKey,
			ReleaseStage: stage,
			// The import paths for the Go packages containing your source files
			ProjectPackages: []string{"main", "github.com/org/myapp"},
			// more configuration options
		})
	}

}
func main() {
	initialize()
	runTickers()
}
func runTickers() {
	var wg sync.WaitGroup
	activeTicker := time.NewTicker(1 * time.Minute)
	watchTicker := time.NewTicker(1 * time.Second)
	testTicker := time.NewTicker(1 * time.Minute)
	refreshTicker := time.NewTicker(5 * time.Second)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		for _ = range activeTicker.C {
			go checkLeaguesForActiveGames()
		}
	}(&wg)
	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		for _ = range watchTicker.C {
			go watchActiveGames()
		}
	}(&wg)
	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		for _ = range testTicker.C {
			go sendTestGoal()
		}
	}(&wg)
	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		for _ = range refreshTicker.C {
			if needRefresh {

				go checkLeaguesForActiveGames()
				needRefresh = false
			}
		}
	}(&wg)
	wg.Wait()

}
func initialize() {

	logger.Info("Puck Drop! Initializing Goalfeed Process")
	dbHost := os.Getenv("DB_HOST")
	logger.Info(fmt.Sprintf("DB_HOST: %s", dbHost))
	redis := os.Getenv("REDIS_HOST")
	logger.Info(fmt.Sprintf("REDIS_HOST: %s", redis))

	logger.Info("Creating League Services")
	// leagueServices[models.LeagueIdIIHF] = iihf.IIHFService{
	// 	Client: iihfClients.IIHFApiClient{},
	// }
	leagueServices[models.LeagueIdNHL] = nhl.NHLService{
		Client: nhlClients.NHLApiClient{},
	}
	leagueServices[models.LeagueIdMLB] = mlb.MLBService{
		Client: mlbClients.MLBApiClient{},
	}
	logger.Info("Initializing Active Games")
	checkLeaguesForActiveGames()
}
func checkLeaguesForActiveGames() {
	logger.Info("Updating Active Games")
	for _, service := range leagueServices {
		go checkForNewActiveGames(service)
	}
}
func checkForNewActiveGames(service leagues.ILeagueService) {
	logger.Info(fmt.Sprintf("Checking for active %s games", service.GetLeagueName()))
	var gamesChan chan []models.Game = make(chan []models.Game)
	go service.GetActiveGames(gamesChan)
	games := <-gamesChan
	for _, game := range games {
		if !gameIsMonitored(game) {
			logger.Info(fmt.Sprintf("Adding %s game (%s @ %s) to active monitored games", service.GetLeagueName(), game.CurrentState.Away.Team.TeamCode, game.CurrentState.Home.Team.TeamCode))
			redis.SetGame(game)
			redis.AppendActiveGame(game)
		}
	}
}
func gameIsMonitored(game models.Game) bool {
	for _, activeGameKey := range redis.GetActiveGameKeys() {
		if activeGameKey == game.GetGameKey() {
			return true
		}
	}
	return false
}
func watchActiveGames() {
	for _, gameKey := range redis.GetActiveGameKeys() {
		go checkGame(gameKey)
	}
}
func checkGame(gameKey string) {
	game, err := redis.GetGameByGameKey(gameKey)
	if err != nil {
		logger.Error(err.Error())
		logger.Error(fmt.Sprintf("[%s] Game not found, skipping", gameKey))
		redis.DeleteActiveGameKey(gameKey)
		needRefresh = true
		return
	}
	service := leagueServices[int(game.LeagueId)]
	// if game.IsFetching {
	// 	log.Println(fmt.Sprintf("[%s - %s @ %s] Already Fetching... skipping", service.GetLeagueName(), game.CurrentState.Away.Team.TeamCode, game.CurrentState.Home.Team.TeamCode))
	// 	return
	// }
	logger.Info(fmt.Sprintf("[%s - %s @ %s] Checking", service.GetLeagueName(), game.CurrentState.Away.Team.TeamCode, game.CurrentState.Home.Team.TeamCode))
	logger.Debug(fmt.Sprintf("[%s - %s @ %s] Setting IsFetching to true", service.GetLeagueName(), game.CurrentState.Away.Team.TeamCode, game.CurrentState.Home.Team.TeamCode))
	game.IsFetching = true
	redis.SetGame(game)

	logger.Info(fmt.Sprintf("[%s - %s @ %s] Getting Update", service.GetLeagueName(), game.CurrentState.Away.Team.TeamCode, game.CurrentState.Home.Team.TeamCode))
	var updateChan chan models.GameUpdate = make(chan models.GameUpdate)
	var eventChan chan []models.Event = make(chan []models.Event)
	go service.GetGameUpdate(game, updateChan)
	update := <-updateChan
	go service.GetEvents(update, eventChan)
	go fireGoalEvents(eventChan, game)
	game.CurrentState = update.NewState
	if game.CurrentState.Status == models.StatusEnded {
		logger.Info(fmt.Sprintf("[%s - %s @ %s] Game has ended", service.GetLeagueName(), game.CurrentState.Away.Team.TeamCode, game.CurrentState.Home.Team.TeamCode))
		redis.DeleteActiveGame(game)
	} else {
		game.IsFetching = false
		logger.Debug(fmt.Sprintf("[%s - %s @ %s] Setting IsFetching to false", service.GetLeagueName(), game.CurrentState.Away.Team.TeamCode, game.CurrentState.Home.Team.TeamCode))
		redis.SetGame(game)
	}
}

func fireGoalEvents(events chan []models.Event, game models.Game) {
	for _, event := range <-events {
		logger.Info(fmt.Sprintf("Goal %s", event.TeamCode))
		go pusher.SendEvent(event)
		var scoringTeam models.Team
		if event.TeamCode == game.CurrentState.Home.Team.TeamCode {
			scoringTeam = game.CurrentState.Home.Team
		} else {
			scoringTeam = game.CurrentState.Away.Team
		}
		go database.InsertGoal(scoringTeam)
	}
}
func sendTestGoal() {
	logger.Info("Sending test goal")
	go pusher.SendEvent(models.Event{
		TeamCode:   "TEST",
		TeamName:   "TEST",
		LeagueId:   0,
		LeagueName: "TEST",
		TeamHash:   "TESTTEST",
	})
}
