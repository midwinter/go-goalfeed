package pusher

import (
	"encoding/json"
	"github.com/pusher/pusher-http-go"
	"goalfeed/models"
	"goalfeed/utils"
	"os"
	"strconv"
)

var logger = utils.GetLogger()

func SendEvent(event models.Event) {
	pusherClient := pusher.Client{
		AppID:  os.Getenv("PUSHER_APP_ID"),
		Key:    os.Getenv("PUSHER_APP_KEY"),
		Secret: os.Getenv("PUSHER_APP_SECRET"),
		Host:   os.Getenv("PUSHER_APP_HOST"),
	}
	msg, e := json.Marshal(event)
	if e != nil {
		logger.Error(e)
		return
	}
	strMsg := strconv.Quote(string(msg))
	err := pusherClient.Trigger("private-goals", "goal", strMsg)

	if err != nil {
		logger.Error(err)
		return
	}
}
