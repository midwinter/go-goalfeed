package database

import (
	"database/sql"
	"fmt"
	"goalfeed/models"
	"goalfeed/utils"
	"log"
	"os"
	"time"

	sq "github.com/Masterminds/squirrel"
	_ "github.com/go-sql-driver/mysql"
)

var logger = utils.GetLogger()

var db = getClient()

func getClient() *sql.DB {

	connectionString := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_DATABASE"))

	dbClient, err := sql.Open("mysql", connectionString)
	if err != nil {
		panic(err)
	}
	return dbClient
}
func GetOrCreateTeam(team models.Team) models.Team {
	existingTeam, e := GetTeamByExtId(team.ExtID, team.LeagueID)

	if e == nil && existingTeam.ID != 0 {
		if existingTeam.TeamName != team.TeamName {
			go UpdateTeam(team)
		}
		return existingTeam
	}
	newTeam, err := InsertTeam(team)
	if err != nil {
		logger.Error("Could not get or create team from DB", err)
		panic(err)
	}
	return newTeam

}

func GetTeamByExtId(extId string, leagueId int) (models.Team, error) {
	teamQ := sq.Select("*").From("teams").Where(sq.Like{"ext_id": extId}).Where(sq.Eq{"league_id": leagueId}).Limit(1)
	rows, err := teamQ.RunWith(db).Query()
	if err != nil {
		logger.Error(err)
	}
	var team models.Team

	rows.Next()
	err = rows.Scan(&team.ID, &team.TeamCode, &team.TeamName, &team.LeagueID, &team.ExtID)
	rows.Close()
	if err != nil {
		return team, err
	}

	return team, err
}

func InsertTeam(team models.Team) (models.Team, error) {
	teamQ := sq.Insert("teams").
		Columns("team_code", "team_name", "league_id", "ext_id").
		Values(team.TeamCode, team.TeamName, team.LeagueID, team.ExtID)
	_, err := teamQ.RunWith(db).Query()
	if err != nil {
		//TODO, remove or rename
		updatedTeam, err := UpdateTeam(team)
		if err != nil {
			log.Fatal(err)
			panic(err)
		}
		return updatedTeam, nil
	}
	return GetTeamByExtId(team.ExtID, team.LeagueID)
}
func InsertGoal(team models.Team) {
	goalQ := sq.Insert("goals").
		Columns("team_id", "game_id", "timestamp").
		Values(team.ID, 0, int32(time.Now().Unix()))

	_, err := goalQ.RunWith(db).Query()
	if err != nil {
		logger.Error("Unable to log goal")
	}
}

func UpdateTeam(team models.Team) (models.Team, error) {
	teamQ := sq.Update("teams").
		Set("ext_id", team.ExtID).
		Set("team_name", team.TeamName).
		Where("league_id = ? AND team_code like ?", team.LeagueID, team.TeamCode)
	rows, err := teamQ.RunWith(db).Query()
	_ = rows
	if err != nil {
		logger.Error(err)
	}
	return GetTeamByExtId(team.ExtID, team.LeagueID)
}
