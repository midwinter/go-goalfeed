package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"goalfeed/models"
	"goalfeed/utils"
	"os"
	"time"

	redis "github.com/go-redis/redis/v8"
)

var ctx = context.Background()
var logger = utils.GetLogger()

func getClient() *redis.Client {

	return redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_HOST"),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0, // use default DB
	})
}

const ACTIVE_GAME_CODES_KEY = "GoalfeedActiveGamesv1"

func GetActiveGameKeys() []string {
	rdb := getClient()
	gamesJSON, err := rdb.Get(ctx, ACTIVE_GAME_CODES_KEY).Result()
	rdb.Close()
	var activeGameKeys []string
	json.Unmarshal([]byte(gamesJSON), &activeGameKeys)
	if err != nil {
		fmt.Println(err.Error())
		return activeGameKeys
	}
	logger.Debug(gamesJSON)
	return activeGameKeys
}

func SetActiveGameKeys(gameCodes []string) {
	if len(gameCodes) < 1 {
		return
	}

	rdb := getClient()
	gamesByte, _ := json.Marshal(gameCodes)
	err := rdb.Set(ctx, ACTIVE_GAME_CODES_KEY, string(gamesByte), 4*time.Hour).Err()
	rdb.Close()
	if err != nil {
		panic(err)
	}
}

func AppendActiveGame(game models.Game) {
	activeGameKeys := GetActiveGameKeys()
	SetGame(game)
	SetActiveGameKeys(append(activeGameKeys, game.GetGameKey()))
}
func DeleteActiveGame(game models.Game) {
	DeleteActiveGameKey(game.GetGameKey())
}
func DeleteActiveGameKey(gameKey string) {
	activeGameKeys := GetActiveGameKeys()
	for i, gameCode := range activeGameKeys {
		if gameCode == gameKey {
			activeGameKeys = append(activeGameKeys[:i], activeGameKeys[i+1:]...)
		}
	}
	SetActiveGameKeys(activeGameKeys)
}

func GetGameByGameKey(gameCode string) (models.Game, error) {
	rdb := getClient()
	logger.Debug(fmt.Sprintf("Lookup %s ", gameCode))
	gameJSON, err := rdb.Get(ctx, gameCode).Result()
	rdb.Close()
	var game models.Game
	json.Unmarshal([]byte(gameJSON), &game)
	if err != nil {
		return game, err
	}
	logger.Debug(gameJSON)
	return game, nil
}
func SetGame(game models.Game) {
	rdb := getClient()
	logger.Debug(fmt.Sprintf("writing to key %s", game.GetGameKey()))
	gameByte, err := json.Marshal(game)
	if err != nil {
		panic(err)
	}
	logger.Debug(fmt.Sprintf("writing %s to key %s", string(gameByte), game.GetGameKey()))
	err = rdb.Set(ctx, game.GetGameKey(), string(gameByte), 10*time.Minute).Err()
	rdb.Close()
	if err != nil {
		panic(err)
	}
}
